resource "aws_instance" "web" {
  ami           = var.instance_ami
  instance_type = var.instance_type
  associate_public_ip_address = true
  key_name = "${aws_key_pair.ec2-pair.key_name}"

  tags = {
    Name = "Module test"
  }
}

resource "aws_key_pair" "ec2-pair" {
  key_name = "terraform-kp"
  public_key = file("~/.ssh/id_rsa.pub")
  tags = {
    "Name" = "Terraform-module"
  }
}

